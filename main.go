// Copyright © 2018 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"log"

	"gitlab.com/kuzzmi/godget/api"
	"gitlab.com/kuzzmi/godget/app"
	"gitlab.com/kuzzmi/godget/cmd"
	"gitlab.com/kuzzmi/godget/db"
)

func main() {
	db, err := db.New("./data.db")
	if err != nil {
		log.Panic(err)
	}

	api := api.New(db)

	app.Ctx = &app.Context{
		Api: api,
	}

	cmd.Execute()
}
