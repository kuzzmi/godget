package api

import (
	"gitlab.com/kuzzmi/godget/db"
)

type Api interface {
	ListAccounts() ([]*db.AccountWithCurrency, error)
	CreateAccount(acc *db.Account) (*db.Account, error)
	UpdateAccount(id int64, name string, balance int, currencyID int64) (*db.AccountWithCurrency, error)

	ListCurrencies() ([]*db.Currency, error)

	CreateTransaction(tr *db.Transaction) (*db.Transaction, error)

	CreateCategory(cat *db.Category) (*db.Category, error)
	ListCategories() ([]*db.Category, error)

	ListBudgets(month string) ([]*db.MonthlyBudgetWithCategory, error)
	UpdateBudget(id int64, budgeted int, currencyID int64) (*db.MonthlyBudgetWithCategory, error)
}

type apiImpl struct {
	db db.Datastore
}

func New(db *db.DB) Api {
	res := &apiImpl{db}

	return res
}
