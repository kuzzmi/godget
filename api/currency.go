package api

import (
	"gitlab.com/kuzzmi/godget/db"
)

func (api *apiImpl) ListCurrencies() ([]*db.Currency, error) {
	curs, err := api.db.AllCurrencies()

	return curs, err
}
