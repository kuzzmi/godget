package api

import (
	"gitlab.com/kuzzmi/godget/db"
)

func (api *apiImpl) CreateTransaction(tr *db.Transaction) (*db.Transaction, error) {
	_, err := api.db.CreateTransaction(tr)
	if err != nil {
		return nil, err
	}

	fromAcc, err := api.db.GetAccount(tr.FromAccountID)
	if err != nil {
		return nil, err
	}

	fromAcc.Balance =
		fromAcc.Balance - tr.FromAmount

	_, err = api.db.UpdateAccount(fromAcc)
	if err != nil {
		return nil, err
	}

	monthYear := getMonthYearNow()

	fromAccMonthlyBudget, err :=
		api.db.GetMonthlyBudgetByMonthAndCategory(
			monthYear,
			tr.CategoryID,
		)
	if err != nil {
		return nil, err
	}

	fromAccMonthlyBudget.AmountLeft =
		fromAccMonthlyBudget.AmountLeft - tr.FromAmount

	_, err = api.db.UpdateMonthlyBudget(fromAccMonthlyBudget)
	if err != nil {
		return nil, err
	}

	return tr, nil
}
