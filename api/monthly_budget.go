package api

import (
	"gitlab.com/kuzzmi/godget/db"
)

func (api *apiImpl) ListBudgets(month string) ([]*db.MonthlyBudgetWithCategory, error) {
	return api.db.MonthlyBudgetsWithCategoriesByMonth(month)
}

func (api *apiImpl) UpdateBudget(id int64, budgeted int, currencyID int64) (*db.MonthlyBudgetWithCategory, error) {
	bud, err := api.db.GetMonthlyBudgetExtByID(id)
	if err != nil {
		return nil, err
	}

	if budgeted != -1 {
		bud.MonthlyBudget.Budgeted = budgeted
	}

	if currencyID != 0 {
		bud.MonthlyBudget.CurrencyID = currencyID
	}

	_, err = api.db.UpdateMonthlyBudget(bud.MonthlyBudget)
	if err != nil {
		return nil, err
	}

	return bud, nil
}
