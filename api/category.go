package api

import (
	"gitlab.com/kuzzmi/godget/db"
)

func (api *apiImpl) CreateCategory(cat *db.Category) (*db.Category, error) {
	cat, err := api.db.CreateCategory(cat)
	if err != nil {
		return nil, err
	}

	monthYear := getMonthYearNow()

	_, err = api.db.CreateMonthlyBudget(&db.MonthlyBudget{
		Month:      monthYear,
		CategoryID: cat.ID,
	})
	if err != nil {
		return nil, err
	}

	return cat, nil
}

func (api *apiImpl) ListCategories() ([]*db.Category, error) {
	categories, err := api.db.AllCategories()
	if err != nil {
		return nil, err
	}

	return categories, nil
}
