package api

import (
	"gitlab.com/kuzzmi/godget/db"
)

func (api *apiImpl) CreateAccount(acc *db.Account) (*db.Account, error) {
	return api.db.CreateAccount(acc)
}

func (api *apiImpl) ListAccounts() ([]*db.AccountWithCurrency, error) {
	return api.db.AllAccountsWithCurrencies()
}

func (api *apiImpl) UpdateAccount(id int64, name string, balance int, currencyID int64) (*db.AccountWithCurrency, error) {
	acc, err := api.db.GetAccountExtByID(id)
	if err != nil {
		return nil, err
	}

	if name != "" {
		acc.Account.Name = name
	}

	if balance != -1 {
		acc.Account.Balance = balance
	}

	if currencyID != 0 {
		acc.Account.CurrencyID = currencyID
	}

	_, err = api.db.UpdateAccount(acc.Account)
	if err != nil {
		return nil, err
	}

	return acc, nil
}
