package api

import (
	"strconv"
	"time"
)

func getMonthYear(t time.Time) string {
	month := strconv.Itoa(int(t.Month()))
	year := strconv.Itoa(t.Year())

	monthYear := month + year

	return monthYear
}

func getMonthYearNow() string {
	now := time.Now()
	return getMonthYear(now)
}
