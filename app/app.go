package app

import (
	"gitlab.com/kuzzmi/godget/api"
)

type Context struct {
	Api api.Api
}

var Ctx *Context
