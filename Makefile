PKGS := $(shell go list ./... | grep -v /vendor)
BINARY := godget

.PHONY: linux
linux:
	mkdir -p bin
	go build -o bin/$(BINARY)

.PHONY: release
release: linux
