// Copyright © 2018 Igor Kuzmenko <igor@kuzzmi.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package cmd

import (
	"fmt"

	c "github.com/logrusorgru/aurora"
	"github.com/spf13/cobra"
	"gitlab.com/kuzzmi/godget/app"
)

// currencyListCmd represents the list command
var currencyListCmd = &cobra.Command{
	Use:     "list",
	Short:   "",
	Aliases: []string{"ls"},
	Run: func(cmd *cobra.Command, args []string) {
		currencies, err := app.Ctx.Api.ListCurrencies()
		if err != nil {
			panic(err)
		}
		fmt.Printf("%4s%-25s%-4s\n", c.BgGray("ID").Black().Bold(), c.BgGray(" Name").Black().Bold(), c.BgGray("Code").Black().Bold())
		for _, cur := range currencies {
			fmt.Printf("%4d %-24s %s\n", cur.ID, cur.Name, cur.Code)
		}
	},
}

func init() {
	currencyCmd.AddCommand(currencyListCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// currencyListCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// currencyListCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
