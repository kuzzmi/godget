// Copyright © 2018 Igor Kuzmenko <igor@kuzzmi.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package cmd

import (
	"fmt"

	c "github.com/logrusorgru/aurora"
	"github.com/spf13/cobra"
	"gitlab.com/kuzzmi/godget/app"
)

var accountModID int64
var accountModName string
var accountModBalance int
var accountModCurrency int64

// accountModCmd represents the accountMod command
var accountModCmd = &cobra.Command{
	Use:   "mod",
	Short: "Edits selected account",
	Run: func(cmd *cobra.Command, args []string) {
		a, err := app.Ctx.Api.UpdateAccount(
			accountModID,
			accountModName,
			accountModBalance,
			accountModCurrency,
		)
		if err != nil {
			panic(err)
		}

		fmt.Printf("% 4s%-13s%13s\n", c.BgGray("ID").Black().Bold(), c.BgGray(" Name").Black().Bold(), c.BgGray("Balance").Black().Bold())
		balance := formatAmount(
			a.Currency,
			a.Account.Balance,
		)

		if a.Account.Balance > 0 {
			balance = c.Sprintf(c.Green("% 12s"), balance)
		} else {
			balance = c.Sprintf(c.Red("% 12s"), balance)
		}
		fmt.Printf(
			"% 4d %-12s % 12s\n",
			a.Account.ID,
			a.Account.Name,
			balance,
		)
	},
}

func init() {
	accountCmd.AddCommand(accountModCmd)

	accountModCmd.Flags().Int64Var(&accountModID, "id", 0, "Account to modify")
	accountModCmd.Flags().StringVarP(&accountModName, "name", "n", "", "Set a new name")
	accountModCmd.Flags().IntVarP(&accountModBalance, "balance", "b", -1, "Set a new balance")
	accountModCmd.Flags().Int64VarP(&accountModCurrency, "currency", "c", 0, "Set a new currency")

	accountModCmd.MarkFlagRequired("id")
}
