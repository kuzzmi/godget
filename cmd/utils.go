package cmd

import (
	"fmt"
	c "github.com/logrusorgru/aurora"
	"gitlab.com/kuzzmi/godget/db"
	"strconv"
	"time"
)

func getMonthYear(t time.Time) string {
	month := strconv.Itoa(int(t.Month()))
	year := strconv.Itoa(t.Year())

	monthYear := month + year

	return monthYear
}

func getMonthYearNow() string {
	now := time.Now()
	return getMonthYear(now)
}

func formatAmount(cur *db.Currency, amount int) string {
	amount_ := float64(amount) / 100

	res := fmt.Sprintf(
		cur.Format,
		cur.Symbol,
		amount_,
	)

	return res
}

func colorizeAmount(cur *db.Currency, amount int) string {
	if amount > 0 {
		return c.Sprintf(c.Green("%12s"), formatAmount(cur, amount))
	} else {
		return c.Sprintf(c.Red("%12s"), formatAmount(cur, amount))
	}
}

func makeNameFormat(strSlice []string) string {
	max := 0

	for _, s := range strSlice {
		if len(s) > max {
			max = len(s)
		}
	}

	max = max + 1

	format := fmt.Sprintf("%%-%ds", max)

	return format
}
