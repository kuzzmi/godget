// Copyright © 2018 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"github.com/spf13/cobra"

	ui "github.com/gizak/termui"
)

// categoryCmd represents the category command
var categoryCmd = &cobra.Command{
	Use:     "category",
	Aliases: []string{"cat"},
	Short:   "Control categories",
	Long:    ``,
	Run: func(cmd *cobra.Command, args []string) {
		// fmt.Println("category called")
	},
}

func CategoryUI() []ui.Bufferer {
	strs := []string{
		"[n] New",
		"[l] List",
		"[b] Back",
	}

	ls := ui.NewList()
	ls.Items = strs
	ls.ItemFgColor = ui.ColorYellow
	ls.BorderLabel = "List"
	ls.Height = 7
	ls.Width = 25
	ls.Y = 0

	return []ui.Bufferer{
		ls,
	}
}

func init() {
	RootCmd.AddCommand(categoryCmd)
}
