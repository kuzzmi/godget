// Copyright © 2018 Igor Kuzmenko <igor@kuzzmi.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package cmd

import (
	"bufio"
	"encoding/csv"
	"io"
	"os"
	"strconv"

	"github.com/spf13/cobra"
	"gitlab.com/kuzzmi/godget/db"
	// "gitlab.com/kuzzmi/godget/api"
)

var filename string
var fromAccountID int64

// importCmd represents the import command
var importCmd = &cobra.Command{
	Use:   "import",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		csvFile, _ := os.Open(filename)
		reader := csv.NewReader(bufio.NewReader(csvFile))
		reader.Comma = ';'
		var txs []*db.Transaction
		reader.Read()
		for {
			line, error := reader.Read()
			if error == io.EOF {
				break
			} else if error != nil {
				panic(error)
			}
			fromAmount, _ := strconv.ParseFloat(line[10], 32)
			txs = append(
				txs,
				&db.Transaction{
					FromAccountID: fromAccountID,
					FromAmount:    int(fromAmount * 100),
					Comment:       line[4],
				},
			)
		}
		// txsJson, _ := json.Marshal(txs)
		// fmt.Println(string(txsJson))
	},
}

func init() {
	RootCmd.AddCommand(importCmd)

	importCmd.Flags().StringVarP(&filename, "path", "p", "", "Path to CSV file to import")
	importCmd.Flags().Int64VarP(&fromAccountID, "account", "a", 0, "Account to which belong all imported transactions")
}
