// Copyright © 2018 Igor Kuzmenko <igor@kuzzmi.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package cmd

import (
	"fmt"
	"time"

	c "github.com/logrusorgru/aurora"
	"github.com/spf13/cobra"
	"gitlab.com/kuzzmi/godget/app"
)

var budgetListMonthDiff int

// budgetListCmd represents the budgetList command
var budgetListCmd = &cobra.Command{
	Use:     "list",
	Short:   "A brief description of your command",
	Aliases: []string{"ls"},
	Run: func(cmd *cobra.Command, args []string) {
		now := time.Now()
		month := now.AddDate(0, budgetListMonthDiff, 0)
		budgets, err := app.Ctx.Api.ListBudgets(getMonthYear(month))
		if err != nil {
			fmt.Println("Failed to get categories")
			panic(err)
			return
		}

		strings := []string{}
		for _, b := range budgets {
			strings = append(
				strings,
				b.Category.Name,
			)
		}

		nameFmt := makeNameFormat(strings)

		fmt.Printf("% 10s% 5d% 35s\n",
			c.BgGreen(month.Month()).Black(),
			c.BgGreen(month.Year()).Black(),
			c.BgGreen(""),
		)
		fmt.Printf("% 4s%s%14s%13s\n",
			c.BgGray("ID").Black().Bold(),
			c.Sprintf(c.BgGray(nameFmt).Black().Bold(), " Name"),
			c.BgGray("Left").Black().Bold(),
			c.BgGray("Budgeted").Black().Bold(),
		)
		for _, b := range budgets {
			if b.Currency.Code != "" {
				budgeted := colorizeAmount(
					b.Currency,
					b.MonthlyBudget.Budgeted,
				)
				left := colorizeAmount(
					b.Currency,
					b.MonthlyBudget.AmountLeft,
				)
				name := fmt.Sprintf(nameFmt, b.Category.Name)
				fmt.Printf(
					"% 4d %s % 12s % 12s\n",
					b.MonthlyBudget.ID,
					name,
					left,
					budgeted,
				)
			} else {
				name := fmt.Sprintf(nameFmt, b.Category.Name)
				fmt.Printf(
					"% 4d %s % 12s % 12s\n",
					b.MonthlyBudget.ID,
					name,
					"-",
					"-",
				)
			}
		}
	},
}

func init() {
	budgetCmd.AddCommand(budgetListCmd)

	budgetListCmd.Flags().IntVarP(&budgetListMonthDiff, "look-month", "n", 0, "Check budget for X months forward or backward")
}
