// Copyright © 2018 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"

	"github.com/rivo/tview"
	"github.com/spf13/cobra"
	"gitlab.com/kuzzmi/godget/app"
	"gitlab.com/kuzzmi/godget/db"
)

var tr *db.Transaction

// transactionNewCmd represents the transactionNew command
var transactionNewCmd = &cobra.Command{
	Use:   "new",
	Short: "A brief description of your command",
	Long:  ``,
	Run:   CreateTransaction,
}

func CreateTransaction(cmd *cobra.Command, args []string) {
	_, err := app.Ctx.Api.CreateTransaction(tr)

	if err != nil {
		fmt.Println("Cannot save transaction: ", err)
	} else {
		fmt.Println("Transaction saved")
	}
}

func transactionNewUI() {
	// ui := tview.NewApplication()
	// form := tview.NewForm().
	// 	AddDropDown("Title", []string{"Mr.", "Ms.", "Mrs.", "Dr.", "Prof."}, 0, nil).
	// 	AddInputField("First name", "", 20, nil, nil).
	// 	AddInputField("Last name", "", 20, nil, nil).
	// 	AddCheckbox("Age 18+", false, nil).
	// 	AddPasswordField("Password", "", 10, '*', nil).
	// 	AddButton("Save", nil).
	// 	AddButton("Quit", func() {
	// 		ui.Stop()
	// 	})
	// form.SetBorder(true).SetTitle("Enter some data").SetTitleAlign(tview.AlignLeft)
	// if err := ui.SetRoot(form, true).Run(); err != nil {
	// 	panic(err)
	// }
	ui := tview.NewApplication()

	categories, err := app.Ctx.Api.ListCategories()
	if err != nil {
		ui.Stop()
		return
	}

	var categoriesOpts []string
	for _, c := range categories {
		categoriesOpts = append(categoriesOpts, c.Name)
	}

	currencies, err := app.Ctx.Api.ListCurrencies()
	if err != nil {
		ui.Stop()
		return
	}
	var currenciesOpts []string
	for _, c := range currencies {
		currenciesOpts = append(currenciesOpts, c.Code)
	}

	// TODO: Optimize
	accounts, err := app.Ctx.Api.ListAccounts()
	if err != nil {
		ui.Stop()
		return
	}
	var accountsOpts []string
	for _, a := range accounts {
		accountsOpts = append(accountsOpts, a.Account.Name)
	}

	tr = &db.Transaction{}

	pages := tview.NewPages()

	form := tview.NewForm().
		AddDropDown("Account", accountsOpts, 0, func(opt string, index int) {
			tr.FromAccountID = accounts[index].Account.ID
		}).
		AddInputField("Amount spent", "0", 20, tview.InputFieldInteger, nil).
		AddDropDown("Currency", currenciesOpts, 0, func(opt string, index int) {
			tr.OriginalCurrencyID = currencies[index].ID
		}).
		AddDropDown("Category", categoriesOpts, 0, func(opt string, index int) {
			tr.CategoryID = categories[index].ID
		}).
		AddButton("Save", func() {
			tv1 := tview.NewModal().SetText("Please wait...")
			pages.AddPage("modal1", tv1, true, true)
			ui.ForceDraw()

			_, err := app.Ctx.Api.CreateTransaction(tr)

			if err != nil {
				pages.HidePage("modal1")
				tv2 := tview.NewModal().
					SetText("Error").
					AddButtons([]string{"Okay"}).
					SetDoneFunc(func(buttonIndex int, buttonLabel string) {
						if buttonLabel == "Okay" {
							ui.Stop()
						}
					})
				pages.AddPage("modal2", tv2, true, true)
				ui.ForceDraw()
			} else {
				ui.Stop()
			}
		}).
		AddButton("Quit", func() {
			ui.Stop()
		})

	form.SetBorder(true).SetTitle("Create new transaction").SetTitleAlign(tview.AlignLeft)

	pages.AddPage("form", form, true, true)

	if err := ui.SetRoot(pages, true).Run(); err != nil {
		panic(err)
	}
}

func init() {
	transactionCmd.AddCommand(transactionNewCmd)

	tr = &db.Transaction{}

	// Mandatory flags
	transactionNewCmd.Flags().Int64VarP(&tr.FromAccountID, "from", "f", 0, "From which account")
	transactionNewCmd.Flags().IntVarP(&tr.FromAmount, "amount", "a", 0, "Amount payed in the account currency")
	transactionNewCmd.Flags().Int64VarP(&tr.CategoryID, "category", "c", 0, "Transaction category")
	transactionNewCmd.MarkFlagRequired("from")
	transactionNewCmd.MarkFlagRequired("amount")
	transactionNewCmd.MarkFlagRequired("category")

	// Optional
	transactionNewCmd.Flags().IntVarP(&tr.OriginalFromAmount, "original-amount", "o", 0, "Originally payed amount")
	transactionNewCmd.Flags().Int64VarP(&tr.OriginalCurrencyID, "original-currency", "u", 0, "Originally payed currency ID")

	// Transfer flags
	transactionNewCmd.Flags().IntVarP(&tr.ToAmount, "to", "t", 0, "Transferred amount")
	transactionNewCmd.Flags().Int64VarP(&tr.ToAccountID, "to-amount", "m", 0, "Transferred to account")
}
