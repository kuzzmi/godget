// Copyright © 2018 Igor Kuzmenko <igor@kuzzmi.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/kuzzmi/godget/app"
)

var budgetModID int64
var budgetModBudgeted int
var budgetModCurrencyID int64

// budgetModCmd represents the budgetMod command
var budgetModCmd = &cobra.Command{
	Use:   "mod",
	Short: "A brief description of your command",
	Run: func(cmd *cobra.Command, args []string) {
		bud, err := app.Ctx.Api.UpdateBudget(
			budgetModID,
			budgetModBudgeted,
			budgetModCurrencyID,
		)
		if err != nil {
			panic(err)
		}
		budgeted := formatAmount(
			bud.Currency,
			bud.MonthlyBudget.Budgeted,
		)
		left := formatAmount(
			bud.Currency,
			bud.MonthlyBudget.AmountLeft,
		)
		fmt.Printf(
			"% 4d %12s % 12s % 12s\n",
			bud.MonthlyBudget.ID,
			bud.Category.Name,
			left,
			budgeted,
		)
	},
}

func init() {
	budgetCmd.AddCommand(budgetModCmd)

	budgetModCmd.Flags().Int64Var(&budgetModID, "id", 0, "Budget ID to modify")
	budgetModCmd.Flags().IntVarP(&budgetModBudgeted, "budgeted", "b", -1, "Budgeted for this month")
	budgetModCmd.Flags().Int64VarP(&budgetModCurrencyID, "currency", "c", 0, "Currency for this month's budget")

	budgetModCmd.MarkFlagRequired("id")
}
