// Copyright © 2018 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"

	c "github.com/logrusorgru/aurora"
	"github.com/spf13/cobra"
	"gitlab.com/kuzzmi/godget/app"
	"gitlab.com/kuzzmi/godget/db"
)

// var currencyCode string
var acc *db.Account

var accountNewCmd = &cobra.Command{
	Use:   "new [name]",
	Short: "Creates a new account",
	Long:  `Creates a new account.`,
	Run: func(cmd *cobra.Command, args []string) {
		acc, err := app.Ctx.Api.CreateAccount(acc)

		if err != nil {
			fmt.Println("Cannot save account: ", err)
		} else {
			fmt.Printf(c.Sprintf(c.Green("Created account %d.\n"), acc.ID))
		}
	},
}

func init() {
	accountCmd.AddCommand(accountNewCmd)

	acc = &db.Account{}

	accountNewCmd.Flags().Int64VarP(&acc.CurrencyID, "currency", "c", 1, "Primary currency")
	accountNewCmd.MarkFlagRequired("currency")
	accountNewCmd.Flags().IntVarP(&acc.Balance, "balance", "b", 0, "Opening balance")
	accountNewCmd.Flags().StringVarP(&acc.Name, "name", "n", "", "Name")
	accountNewCmd.MarkFlagRequired("name")
}
