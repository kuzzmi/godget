// Copyright © 2018 Igor Kuzmenko <igor@kuzzmi.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package cmd

import (
	"fmt"

	c "github.com/logrusorgru/aurora"
	"github.com/spf13/cobra"
	"gitlab.com/kuzzmi/godget/app"
)

// accountListCmd represents the new command
var accountListCmd = &cobra.Command{
	Use:     "list",
	Aliases: []string{"ls"},
	Short:   "Lists accounts",
	Long:    "Lists all accounts.",
	Run: func(cmd *cobra.Command, args []string) {
		accounts, err := app.Ctx.Api.ListAccounts()
		if err != nil {
			panic(err)
		}

		fmt.Printf("% 4s%-13s%13s\n", c.BgGray("ID").Black().Bold(), c.BgGray(" Name").Black().Bold(), c.BgGray("Balance").Black().Bold())
		for _, a := range accounts {
			balance := colorizeAmount(
				a.Currency,
				a.Account.Balance,
			)

			fmt.Printf(
				"% 4d %-12s % 12s\n",
				a.Account.ID,
				a.Account.Name,
				balance,
			)
		}
	},
}

func init() {
	accountCmd.AddCommand(accountListCmd)
}
