// Copyright © 2018 Igor Kuzmenko <igor@kuzzmi.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package cmd

import (
	"fmt"

	c "github.com/logrusorgru/aurora"
	"github.com/spf13/cobra"
	"gitlab.com/kuzzmi/godget/app"
	"gitlab.com/kuzzmi/godget/db"
)

var cat *db.Category

// categoryNewCmd represents the categoryNew command
var categoryNewCmd = &cobra.Command{
	Use:   "new",
	Short: "Create new category",
	Run: func(cmd *cobra.Command, args []string) {
		cat, err := app.Ctx.Api.CreateCategory(cat)

		if err != nil {
			fmt.Println("Cannot save category: ", err)
		} else {
			fmt.Printf(c.Sprintf(c.Green("Created category %d.\n"), cat.ID))
		}
	},
}

func init() {
	categoryCmd.AddCommand(categoryNewCmd)

	cat = &db.Category{}

	categoryNewCmd.Flags().StringVarP(&cat.Name, "name", "n", "", "Category name")
	categoryNewCmd.MarkFlagRequired("name")
	categoryNewCmd.Flags().IntVarP(&cat.ParentCategoryID, "parent", "p", 0, "Parent category")
	categoryNewCmd.Flags().IntVarP(&cat.IsExpense, "expense", "e", 0, "This category combines expenses")
}
