package db

import (
	"time"
)

type Transaction struct {
	ID int `json:"id"`

	FromAccountID int64 `json:"fromAccountID"` // from which account the transaction was performed
	ToAccountID   int64 `json:"toAccountID"`
	PayeeID       int64 `json:"payeeID"`
	CategoryID    int64 `json:"categoryID"`

	FromAmount int `json:"fromAmount"` // amount that left the account
	ToAmount   int `json:"toAmount"`   // amount that entered anothe account

	OriginalFromAmount int   `json:"originalFromAmount"` // amount that was originally payed
	OriginalCurrencyID int64 `json:"originalCurrencyID"` // currency that was used for payment, can be different from account currency

	Comment   string `json:"comment"`
	UpdatedAt string `json:"updatedAt"`
	CreatedAt string `json:"createdAt"`
}

/*
	Example:

	Payed 28 UAH from a USD credit card:

	Before exchange rate is known:
	Transaction{
		FromAccount: USD Card
		FromAmount: 0,
		OriginalFromAmount: 28,
		OriginalCurrency: UAH
	}

	Payed 5 USD from USD card:
	Transaction{
		FromAccount: USD Card
		FromAmount: 5,
		OriginalFromAmount: 0,
		OriginalCurrency: NULL
	}
*/

func (db *DB) CreateTransactionTable() error {
	statement, err := db.Prepare(`
		CREATE TABLE IF NOT EXISTS
			transact
		(
			id INTEGER PRIMARY KEY NOT NULL,

			from_account_id INTEGER NOT NULL,
			to_account_id INTEGER,
			payee_id INTEGER,
			category_id INTEGER,

			from_amount INTEGER NOT NULL DEFAULT 0,
			to_amount INTEGER DEFAULT 0,

			original_from_amount INTEGER,
			original_currency_id INTEGER,

			comment TEXT,
			updated_at INTEGER NOT NULL DEFAULT 0,
			created_at INTEGER NOT NULL DEFAULT 0
		);
	`)
	if err != nil {
		return err
	}

	statement.Exec()

	return err
}

func (db *DB) AllTransactions() ([]*Transaction, error) {
	rows, err := db.Query(`
		SELECT
			id,
			from_account_id,
			to_account_id,
			payee_id,

			from_amount,
			to_amount,

			original_from_amount,
			original_currency_id,

			comment,
			updated_at,
			created_at
		FROM
			transact
		`)
	if err != nil {
		return nil, err
	}

	transactions := []*Transaction{}
	for rows.Next() {
		transaction := &Transaction{}
		rows.Scan(
			&transaction.ID,
			&transaction.FromAccountID,
			&transaction.ToAccountID,
			&transaction.PayeeID,
			&transaction.FromAmount,
			&transaction.ToAmount,
			&transaction.OriginalFromAmount,
			&transaction.OriginalCurrencyID,
			&transaction.Comment,
			&transaction.UpdatedAt,
			&transaction.CreatedAt,
		)

		transactions = append(
			transactions,
			transaction,
		)
	}

	return transactions, err
}

func (db *DB) CreateTransaction(tr *Transaction) (*Transaction, error) {
	statement, err := db.Prepare(`
		INSERT INTO
			transact
		(
			from_account_id,
			to_account_id,
			payee_id,
			category_id,
			from_amount,
			to_amount,
			original_from_amount,
			original_currency_id,
			comment,
			updated_at,
			created_at
		)
		VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
	`)
	if err != nil {
		return nil, err
	}

	statement.Exec(
		tr.FromAccountID,
		tr.ToAccountID,
		tr.PayeeID,
		tr.CategoryID,
		tr.FromAmount,
		tr.ToAmount,
		tr.OriginalFromAmount,
		tr.OriginalCurrencyID,
		tr.Comment,
		time.Now(),
		time.Now(),
	)

	return tr, nil
}
