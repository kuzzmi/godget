package db

import (
	"time"
)

type MonthlyBudget struct {
	ID         int64  `json:"id"`
	Month      string `json:"month"`
	CategoryID int64  `json:"categoryID"`
	Budgeted   int    `json:"budgeted"`
	AmountLeft int    `json:"amountLeft"`
	CurrencyID int64  `json:"currencyID"`
	UpdatedAt  string `json:"updatedAt"`
	CreatedAt  string `json:"createdAt"`
}

type MonthlyBudgetWithCategory struct {
	MonthlyBudget *MonthlyBudget
	Category      *Category
	Currency      *Currency
}

func (db *DB) CreateMonthlyBudgetTable() error {
	statement, err := db.Prepare(`
		CREATE TABLE IF NOT EXISTS
			monthly_budget
		(
			id INTEGER PRIMARY KEY NOT NULL,
			month STRING NOT NULL,
			category_id INTEGER NOT NULL,
			budgeted INTEGER NOT NULL DEFAULT 0,
			amount_left INTEGER NOT NULL DEFAULT 0,
			currency_id INTEGER NOT NULL,
			updated_at INTEGER NOT NULL DEFAULT 0,
			created_at INTEGER NOT NULL DEFAULT 0
		);
	`)
	statement.Exec()

	return err
}

func (db *DB) AllMonthlyBudgets() ([]*MonthlyBudget, error) {
	rows, err := db.Query(`
		SELECT
			id,
			month,
			category_id,
			budgeted,
			amount_left,
			currency_id,
			updated_at,
			created_at
		FROM
			monthly_budget
	`)
	if err != nil {
		return nil, err
	}

	monthlyBudgets := []*MonthlyBudget{}
	for rows.Next() {
		monthlyBudget := &MonthlyBudget{}
		rows.Scan(
			&monthlyBudget.ID,
			&monthlyBudget.Month,
			&monthlyBudget.CategoryID,
			&monthlyBudget.Budgeted,
			&monthlyBudget.AmountLeft,
			&monthlyBudget.CurrencyID,
			&monthlyBudget.UpdatedAt,
			&monthlyBudget.CreatedAt,
		)
		monthlyBudgets = append(
			monthlyBudgets,
			monthlyBudget,
		)
	}

	return monthlyBudgets, nil
}

func (db *DB) MonthlyBudgetsWithCategoriesByMonth(month string) ([]*MonthlyBudgetWithCategory, error) {
	rows, err := db.Query(`
		SELECT
			cat.id,
			cat.name,
			cat.parent_category_id,
			cat.is_expense,
			cat.updated_at,
			cat.created_at,
			mb.id,
			mb.month,
			mb.category_id,
			mb.budgeted,
			mb.amount_left,
			mb.currency_id,
			mb.updated_at,
			mb.created_at,
			cur.id,
			cur.name,
			cur.code,
			cur.symbol,
			cur.format
		FROM
			category cat
		LEFT JOIN monthly_budget mb ON mb."month" = ? AND cat.id = mb.category_id
		LEFT JOIN currency cur ON cur.id = mb.currency_id
	`, month)
	if err != nil {
		return nil, err
	}

	monthlyBudgetsWithCategories := []*MonthlyBudgetWithCategory{}
	for rows.Next() {
		monthlyBudget := &MonthlyBudget{}
		category := &Category{}
		currency := &Currency{}
		rows.Scan(
			&category.ID,
			&category.Name,
			&category.ParentCategoryID,
			&category.IsExpense,
			&category.UpdatedAt,
			&category.CreatedAt,
			&monthlyBudget.ID,
			&monthlyBudget.Month,
			&monthlyBudget.CategoryID,
			&monthlyBudget.Budgeted,
			&monthlyBudget.AmountLeft,
			&monthlyBudget.CurrencyID,
			&monthlyBudget.UpdatedAt,
			&monthlyBudget.CreatedAt,
			&currency.ID,
			&currency.Name,
			&currency.Code,
			&currency.Symbol,
			&currency.Format,
		)
		monthlyBudgetsWithCategories = append(
			monthlyBudgetsWithCategories,
			&MonthlyBudgetWithCategory{
				MonthlyBudget: monthlyBudget,
				Category:      category,
				Currency:      currency,
			},
		)
	}

	for i, mb := range monthlyBudgetsWithCategories {
		if mb.MonthlyBudget.ID == 0 {
			mb_ := &MonthlyBudget{
				CategoryID: mb.Category.ID,
				Month:      month,
			}
			mb.MonthlyBudget, err = db.CreateMonthlyBudget(mb_)
			monthlyBudgetsWithCategories[i] = mb
		}
	}

	return monthlyBudgetsWithCategories, nil
}

func (db *DB) GetMonthlyBudgetExtByID(id int64) (*MonthlyBudgetWithCategory, error) {
	rows, err := db.Query(`
		SELECT
			mb.id,
			mb.month,
			mb.category_id,
			mb.budgeted,
			mb.amount_left,
			mb.currency_id,
			mb.updated_at,
			mb.created_at,
			cat.id,
			cat.name,
			cat.parent_category_id,
			cat.is_expense,
			cat.updated_at,
			cat.created_at,
			cur.id,
			cur.name,
			cur.code,
			cur.symbol,
			cur.format
		FROM
			monthly_budget mb
		JOIN category cat ON cat.id = mb.category_id
		LEFT JOIN currency cur ON cur.id = mb.currency_id
		WHERE
			mb.id = ?
	`, id)
	if err != nil {
		return nil, err
	}

	monthlyBudgetExt := &MonthlyBudgetWithCategory{}
	for rows.Next() {
		monthlyBudget := &MonthlyBudget{}
		category := &Category{}
		currency := &Currency{}
		rows.Scan(
			&monthlyBudget.ID,
			&monthlyBudget.Month,
			&monthlyBudget.CategoryID,
			&monthlyBudget.Budgeted,
			&monthlyBudget.AmountLeft,
			&monthlyBudget.CurrencyID,
			&monthlyBudget.UpdatedAt,
			&monthlyBudget.CreatedAt,
			&category.ID,
			&category.Name,
			&category.ParentCategoryID,
			&category.IsExpense,
			&category.UpdatedAt,
			&category.CreatedAt,
			&currency.ID,
			&currency.Name,
			&currency.Code,
			&currency.Symbol,
			&currency.Format,
		)
		monthlyBudgetExt = &MonthlyBudgetWithCategory{
			MonthlyBudget: monthlyBudget,
			Category:      category,
			Currency:      currency,
		}
	}

	return monthlyBudgetExt, nil
}

func (db *DB) CreateMonthlyBudget(mb *MonthlyBudget) (*MonthlyBudget, error) {
	statement, err := db.Prepare(`
		INSERT INTO
			monthly_budget
		(
			month,
			category_id,
			budgeted,
			amount_left,
			currency_id,
			created_at
		)
		VALUES (?, ?, ?, ?, ?, ?)
	`)
	if err != nil {
		return nil, err
	}

	statement.Exec(
		mb.Month,
		mb.CategoryID,
		mb.Budgeted,
		mb.AmountLeft,
		mb.CurrencyID,
		time.Now(),
	)

	return mb, nil
}

func (db *DB) GetMonthlyBudgetByMonthAndCategory(month string, categoryID int64) (*MonthlyBudget, error) {
	rows, err := db.Query(`
		SELECT
			id,
			month,
			category_id,
			budgeted,
			amount_left,
			currency_id,
			updated_at,
			created_at
		FROM
			monthly_budget
		WHERE
			month = ?
		AND
			category_id = ?
	`, month, categoryID)
	if err != nil {
		return nil, err
	}

	monthlyBudget := &MonthlyBudget{}
	for rows.Next() {
		rows.Scan(
			&monthlyBudget.ID,
			&monthlyBudget.Month,
			&monthlyBudget.CategoryID,
			&monthlyBudget.Budgeted,
			&monthlyBudget.AmountLeft,
			&monthlyBudget.CurrencyID,
			&monthlyBudget.UpdatedAt,
			&monthlyBudget.CreatedAt,
		)
	}

	return monthlyBudget, nil
}

func (db *DB) UpdateMonthlyBudget(mb *MonthlyBudget) (*MonthlyBudget, error) {
	statement, err := db.Prepare(`
		UPDATE
			monthly_budget
		SET
			budgeted = ?,
			amount_left = ?,
			currency_id = ?,
			updated_at = ?
		WHERE
			id = ?
	`)
	if err != nil {
		return nil, err
	}

	res, _ := statement.Exec(
		mb.Budgeted,
		mb.AmountLeft,
		mb.CurrencyID,
		time.Now(),
		mb.ID,
	)

	id, err := res.LastInsertId()
	if err != nil {
		return nil, err
	}
	mb.ID = id

	return mb, nil
}
