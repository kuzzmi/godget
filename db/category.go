package db

import (
	"time"
)

type Category struct {
	ID               int64  `json:"id"`
	Name             string `json:"name"`
	ParentCategoryID int    `json:"parentCategoryID"`
	IsExpense        int    `json:"isExpense"`
	UpdatedAt        string `json:"updatedAt"`
	CreatedAt        string `json:"createdAt"`
}

func (db *DB) CreateCategoryTable() error {
	statement, err := db.Prepare(`
		CREATE TABLE IF NOT EXISTS
			category
		(
			id INTEGER PRIMARY KEY NOT NULL,
			name TEXT NOT NULL UNIQUE,
			parent_category_id INTEGER,
			is_expense BOOL DEFAULT TRUE,
			updated_at INTEGER NOT NULL DEFAULT 0,
			created_at INTEGER NOT NULL DEFAULT 0
		);
	`)
	statement.Exec()

	return err
}

func (db *DB) AllCategories() ([]*Category, error) {
	rows, err := db.Query(`
		SELECT
			id,
			name,
			parent_category_id,
			is_expense,
			updated_at,
			created_at
		FROM
			category
	`)
	if err != nil {
		return nil, err
	}

	categories := []*Category{}
	for rows.Next() {
		category := &Category{}
		rows.Scan(
			&category.ID,
			&category.Name,
			&category.ParentCategoryID,
			&category.IsExpense,
			&category.UpdatedAt,
			&category.CreatedAt,
		)
		categories = append(
			categories,
			category,
		)
	}

	return categories, nil
}

func (db *DB) CreateCategory(cat *Category) (*Category, error) {
	statement, err := db.Prepare(`
		INSERT INTO
			category
		(
			name,
			parent_category_id,
			is_expense,
			created_at
		)
		VALUES (?, ?, ?, ?)
	`)
	if err != nil {
		return nil, err
	}

	res, err := statement.Exec(
		cat.Name,
		cat.ParentCategoryID,
		cat.IsExpense,
		time.Now(),
	)
	if err != nil {
		return nil, err
	}

	id, err := res.LastInsertId()
	if err != nil {
		return nil, err
	}
	cat.ID = id

	return cat, nil
}
