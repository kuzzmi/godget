package db

import (
	"time"
)

type Account struct {
	ID         int64  `json:"id"`
	Name       string `json:"name"`
	Balance    int    `json:"balance"`
	CurrencyID int64  `json:"currencyID"`
	UpdatedAt  string `json:"updatedAt"`
	CreatedAt  string `json:"createdAt"`
}

type AccountWithCurrency struct {
	Account  *Account
	Currency *Currency
}

func (db *DB) CreateAccountTable() error {
	statement, err := db.Prepare(`
		CREATE TABLE IF NOT EXISTS
			account
		(
			id INTEGER PRIMARY KEY NOT NULL,
			name TEXT NOT NULL UNIQUE,
			balance INTEGER NOT NULL DEFAULT 0,
			currency_id INTEGER NOT NULL,
			updated_at INTEGER NOT NULL DEFAULT 0,
			created_at INTEGER NOT NULL DEFAULT 0
		);
	`)
	statement.Exec()

	return err
}

func (db *DB) AllAccounts() ([]*Account, error) {
	rows, err := db.Query(`
		SELECT
			id,
			name,
			balance,
			currency_id,
			updated_at,
			created_at
		FROM
			account
		`)
	if err != nil {
		return nil, err
	}

	accounts := []*Account{}
	for rows.Next() {
		account := &Account{}
		rows.Scan(
			&account.ID,
			&account.Name,
			&account.Balance,
			&account.CurrencyID,
			&account.UpdatedAt,
			&account.CreatedAt,
		)
		accounts = append(
			accounts,
			account,
		)
	}

	return accounts, nil
}

func (db *DB) AllAccountsWithCurrencies() ([]*AccountWithCurrency, error) {
	rows, err := db.Query(`
		SELECT
			a.id,
			a.name,
			a.balance,
			a.currency_id,
			a.updated_at,
			a.created_at,
			c.id,
			c.name,
			c.code,
			c.symbol,
			c.format
		FROM
			account a
		JOIN currency c ON c.id = a.currency_id
		`)
	if err != nil {
		return nil, err
	}

	accounts := []*AccountWithCurrency{}
	for rows.Next() {
		account := &Account{}
		currency := &Currency{}
		rows.Scan(
			&account.ID,
			&account.Name,
			&account.Balance,
			&account.CurrencyID,
			&account.UpdatedAt,
			&account.CreatedAt,
			&currency.ID,
			&currency.Name,
			&currency.Code,
			&currency.Symbol,
			&currency.Format,
		)
		accounts = append(
			accounts,
			&AccountWithCurrency{
				Account:  account,
				Currency: currency,
			},
		)
	}

	return accounts, nil
}

func (db *DB) GetAccountExtByID(id int64) (*AccountWithCurrency, error) {
	rows, err := db.Query(`
		SELECT
			a.id,
			a.name,
			a.balance,
			a.currency_id,
			a.updated_at,
			a.created_at,
			c.id,
			c.name,
			c.code,
			c.symbol,
			c.format
		FROM
			account a
		JOIN currency c ON c.id = a.currency_id
		WHERE
			a.id = ?
	`, id)
	if err != nil {
		return nil, err
	}

	res := &AccountWithCurrency{}
	account := &Account{}
	currency := &Currency{}
	for rows.Next() {
		rows.Scan(
			&account.ID,
			&account.Name,
			&account.Balance,
			&account.CurrencyID,
			&account.UpdatedAt,
			&account.CreatedAt,
			&currency.ID,
			&currency.Name,
			&currency.Code,
			&currency.Symbol,
			&currency.Format,
		)
		res.Account = account
		res.Currency = currency
	}

	return res, nil
}

func (db *DB) CreateAccount(acc *Account) (*Account, error) {
	statement, err := db.Prepare(`
		INSERT INTO
			account
		(
			name,
			balance,
			currency_id,
			created_at
		)
		VALUES (?, ?, ?, ?)
	`)
	if err != nil {
		return nil, err
	}

	res, err := statement.Exec(
		acc.Name,
		acc.Balance,
		acc.CurrencyID,
		time.Now(),
	)
	if err != nil {
		return nil, err
	}

	id, err := res.LastInsertId()
	if err != nil {
		return nil, err
	}

	acc.ID = id

	return acc, nil
}

func (db *DB) DeleteAccount(id int64) error {
	panic("Not implemented")
	return nil
}

func (db *DB) GetAccount(id int64) (*Account, error) {
	rows, err := db.Query(`
		SELECT
			id,
			name,
			balance,
			currency_id,
			updated_at,
			created_at
		FROM
			account
		WHERE
			id = ?
	`, id)
	if err != nil {
		return nil, err
	}

	account := &Account{}
	for rows.Next() {
		rows.Scan(
			&account.ID,
			&account.Name,
			&account.Balance,
			&account.CurrencyID,
			&account.UpdatedAt,
			&account.CreatedAt,
		)
	}

	return account, nil
}

func (db *DB) UpdateAccount(acc *Account) (*Account, error) {
	statement, err := db.Prepare(`
		UPDATE
			account
		SET
			name = ?,
			balance = ?,
			currency_id = ?,
			updated_at = ?
		WHERE
			id = ?
	`)
	if err != nil {
		return nil, err
	}

	statement.Exec(
		acc.Name,
		acc.Balance,
		acc.CurrencyID,
		time.Now(),
		acc.ID,
	)

	return acc, nil
}
