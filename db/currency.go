package db

type Currency struct {
	ID     int64  `json:"id"`
	Name   string `json:"name"`
	Code   string `json:"code"`
	Symbol string `json:"symbol"`
	Format string `json:"format"`
}

func (db *DB) CreateCurrencyTable() error {
	statement, err := db.Prepare(`
		CREATE TABLE IF NOT EXISTS
			currency
		(
			id INTEGER PRIMARY KEY NOT NULL,
			name TEXT NOT NULL,
			code TEXT NOT NULL UNIQUE,
			symbol TEXT NOT NULL,
			format TEXT NOT NULL
		);
	`)
	statement.Exec()
	if err != nil {
		return err
	}

	currencies := []*Currency{
		{
			Name:   "United States Dollar",
			Code:   "USD",
			Symbol: "$",
			Format: "%s%.2f",
		},
		{
			Name:   "Ukrainian Hryvna",
			Code:   "UAH",
			Symbol: "UAH",
			Format: "%s%.2f",
		},
		{
			Name:   "Swiss Franc",
			Code:   "CHF",
			Symbol: "CHF",
			Format: "%s%.2f",
		},
	}

	for _, c := range currencies {
		_, err = db.CreateCurrency(c)
		if err != nil {
			return err
		}
	}

	return nil
}

func (db *DB) AllCurrencies() ([]*Currency, error) {
	rows, err := db.Query(`
		SELECT
			id,
			name,
			code,
			symbol,
			format
		FROM
			currency
		`)
	if err != nil {
		return nil, err
	}

	currencies := []*Currency{}
	for rows.Next() {
		currency := &Currency{}
		rows.Scan(
			&currency.ID,
			&currency.Name,
			&currency.Code,
			&currency.Symbol,
			&currency.Format,
		)
		currencies = append(
			currencies,
			currency,
		)
	}

	return currencies, nil
}

func (db *DB) CreateCurrency(cur *Currency) (*Currency, error) {
	statement, err := db.Prepare(`
		INSERT INTO
			currency
		(
			name,
			code,
			symbol,
			format
		)
		VALUES (?, ?, ?, ?)
	`)
	if err != nil {
		return nil, err
	}

	statement.Exec(
		cur.Name,
		cur.Code,
		cur.Symbol,
		cur.Format,
	)

	return cur, nil
}

func (db *DB) GetCurrency(id int64) (*Currency, error) {
	rows, err := db.Query(`
		SELECT
			id,
			name,
			code,
			symbol,
			format
		FROM
			currency
		WHERE
			id = ?
	`, id)
	if err != nil {
		return nil, err
	}

	currency := &Currency{}
	for rows.Next() {
		rows.Scan(
			&currency.ID,
			&currency.Name,
			&currency.Code,
			&currency.Symbol,
			&currency.Format,
		)
	}

	return currency, nil
}
