package db

import (
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
)

type Datastore interface {
	CreateAccountTable() error
	AllAccounts() ([]*Account, error)
	CreateAccount(acc *Account) (*Account, error)
	DeleteAccount(id int64) error
	GetAccount(id int64) (*Account, error)
	GetAccountExtByID(id int64) (*AccountWithCurrency, error)
	UpdateAccount(acc *Account) (*Account, error)

	CreateCurrencyTable() error
	AllCurrencies() ([]*Currency, error)
	CreateCurrency(cur *Currency) (*Currency, error)
	GetCurrency(id int64) (*Currency, error)

	CreateTransactionTable() error
	AllTransactions() ([]*Transaction, error)
	CreateTransaction(tr *Transaction) (*Transaction, error)

	CreateCategoryTable() error
	AllCategories() ([]*Category, error)
	CreateCategory(cat *Category) (*Category, error)

	CreateMonthlyBudgetTable() error
	AllMonthlyBudgets() ([]*MonthlyBudget, error)
	GetMonthlyBudgetByMonthAndCategory(month string, categoryID int64) (*MonthlyBudget, error)
	GetMonthlyBudgetExtByID(id int64) (*MonthlyBudgetWithCategory, error)
	CreateMonthlyBudget(mb *MonthlyBudget) (*MonthlyBudget, error)
	UpdateMonthlyBudget(mb *MonthlyBudget) (*MonthlyBudget, error)

	// Reports
	MonthlyBudgetsWithCategoriesByMonth(month string) ([]*MonthlyBudgetWithCategory, error)
	AllAccountsWithCurrencies() ([]*AccountWithCurrency, error)
}

type DB struct {
	*sql.DB
}

func New(connString string) (*DB, error) {
	// Open up our database connection.
	db, err := sql.Open("sqlite3", connString)

	// if there is an error opening the connection, handle it
	if err != nil {
		return nil, err
	}

	if err = db.Ping(); err != nil {
		return nil, err
	}

	res := &DB{db}

	err = res.CreateAccountTable()
	if err != nil {
		return nil, err
	}

	err = res.CreateCurrencyTable()
	if err != nil {
		return nil, err
	}

	err = res.CreateMonthlyBudgetTable()
	if err != nil {
		return nil, err
	}

	err = res.CreateTransactionTable()
	if err != nil {
		return nil, err
	}

	err = res.CreateCategoryTable()
	if err != nil {
		return nil, err
	}

	return res, nil
}
